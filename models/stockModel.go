package models

import (
  "github.com/jinzhu/gorm"
  "time"
)

// Task "Object
type Stocks struct {
  ID          int       `json:"id" gorm:"AUTO_INCREMENT;primary_key;column:id"`
  Sku         string    `json:"sku"`
  Name        string    `json:"name"`
  Sales       float64   `json:"sales"`
  Purchases   float64   `json:"purchases"`
  CreatedDate time.Time `json:"createdDate"`
  CreatedBy   string    `json:"createdBy"`
  UpdatedDate time.Time `json:"updatedDate"`
  Status      int       `json:"status"`
  Amount      int       `json:"amount"`
}

func (stock *Stocks) BeforeCreate(scope *gorm.Scope) error {
  scope.SetColumn("CreatedDate", time.Now())
  return nil
}

func (stock *Stocks) BeforeUpdate(scope *gorm.Scope) error {
  scope.SetColumn("UpdatedDate", time.Now())
  return nil
}
