package models

import (
  "github.com/jinzhu/gorm"
  "time"
)

// Task "Object
type Transaction struct {
  ID          int       `json:"id" gorm:"AUTO_INCREMENT;primary_key;column:id"`
  StockId     int       `json:"stockId" binding:"required"`
  Types       int       `json:"types"`
  Price       float64   `json:"price"`
  Amount      int       `json:"amount"`
  CreatedBy   string    `json:"createdBy"`
  UpdatedDate time.Time `json:"updatedDate"`
  CreatedDate time.Time `json:"createdDate"`
  Status      int       `json:"status"`
}

func (transaction *Transaction) BeforeCreate(scope *gorm.Scope) error {
  scope.SetColumn("CreatedDate", time.Now())
  return nil
}

func (transaction *Transaction) BeforeUpdate(scope *gorm.Scope) error {
  scope.SetColumn("UpdatedDate", time.Now())
  return nil
}
