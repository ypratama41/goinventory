package db

import (
  // "fmt"
  "github.com/jinzhu/gorm"
  // _ "github.com/jinzhu/gorm/dialects/postgres"
  // _ "github.com/lib/pq"
  _ "github.com/mattn/go-sqlite3"
  "gobigbuddy/models"
  "log"
  "os"
)

var db *gorm.DB
var err error

func getEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

// Init creates a connection to mysql database and
// migrates any new models
// func Init() {
//   user := getEnv("PG_USER", "postgres")
//   password := getEnv("PG_PASSWORD", "pgAdm123")
//   host := getEnv("PG_HOST", "localhost")
//   port := getEnv("PG_PORT", "5432")
//   database := getEnv("PG_DB", "tesDb")

//   dbinfo := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable",
//     user,
//     password,
//     host,
//     port,
//     database,
//   )

//   db, err = gorm.Open("postgres", dbinfo)
//   if err != nil {
//     log.Println("Failed to connect to database")
//     panic(err)
//   }
//   log.Println("Database connected")

//   if !db.HasTable(&models.Task{}) {
//     err := db.CreateTable(&models.Task{})
//     if err != nil {
//       log.Println("Table already exists")
//     }
//   }

//   db.AutoMigrate(&models.Task{})
// }

// sql lite

func Init() {

  db, err = gorm.Open("sqlite3", "./caseStudy.db")
  if err != nil {
    log.Println("Failed to connect to database")
    panic(err)
  }
  log.Println("Database connected")

  if !db.HasTable(&models.Stocks{}) {
    err := db.CreateTable(&models.Stocks{})
    if err != nil {
      log.Println("Table already exists")
    }
  }
  db.AutoMigrate(&models.Stocks{})

  if !db.HasTable(&models.Transaction{}) {
    err := db.CreateTable(&models.Transaction{})
    if err != nil {
      log.Println("Table already exists")
    }
  }
  db.AutoMigrate(&models.Transaction{})
}

//GetDB ...
func GetDB() *gorm.DB {
  return db
}

func CloseDB() {
  db.Close()
}
