package main

import (
  // "github.com/gin-gonic/contrib/jwt"
  "github.com/gin-gonic/gin"
  TaskController "gobigbuddy/controllers"
  db "gobigbuddy/db"
  Middleware "gobigbuddy/middleware"
  "log"
)

func main() {
  log.Println("Starting server..")

  db.Init()

  route := gin.Default()

  login := route.Group("/login")
  {
    login.GET("/", Middleware.CreateToken)
  }

  stock := route.Group("/stock")
  {
    // stock.Use(jwt.Auth(Middleware.Mysupersecretpassword))
    stock.GET("/", TaskController.GetStock)
    stock.GET("/:id", TaskController.GetStockById)
    stock.POST("/", TaskController.CreateStock)
    stock.PUT("/:id", TaskController.UpdateStock)
    stock.DELETE("/:id", TaskController.HardDeleteStock)
    stock.PUT("/:id/soft-delete", TaskController.SoftDeleteStock)
    stock.PUT("/:id/activate", TaskController.ActivateStock)
  }

  transaction := route.Group("/transaction")
  {
    // transaction.Use(jwt.Auth(Middleware.Mysupersecretpassword))
    transaction.GET("/", TaskController.GetTransaction)
    transaction.GET("/:id", TaskController.GetTransactionById)
    transaction.POST("/", TaskController.CreateTransaction)
    transaction.PUT("/:id", TaskController.UpdateTransaction)
    transaction.DELETE("/:id", TaskController.HardDeleteTransaction)
    transaction.PUT("/:id/soft-delete", TaskController.SoftDeleteTransaction)
    transaction.PUT("/:id/activate", TaskController.ActivateTransaction)
  }

  export := route.Group("/export")
  {
    // export.Use(jwt.Auth(Middleware.Mysupersecretpassword))
    export.GET("/stock", TaskController.ExportStock)
    export.GET("/sales-stock", TaskController.ExportSales)
    export.GET("/purchases-stock", TaskController.ExportPurchases)
    export.GET("/profit-loss", TaskController.ExportProfit)
  }

  imports := route.Group("/import")
  {
    // export.Use(jwt.Auth(Middleware.Mysupersecretpassword))
    imports.POST("/stock", TaskController.ImportStockCsv)
  }

  route.Run()
}
