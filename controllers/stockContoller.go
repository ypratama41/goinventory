package controllers

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"github.com/gin-gonic/gin"
	"gobigbuddy/db"
	"gobigbuddy/models"
	"log"
	"math"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"
)

func GetStock(c *gin.Context) {

	var stocks []models.Stocks
	db := db.GetDB()

	db.Where("status = ?", 1).Find(&stocks)

	c.JSON(http.StatusCreated,
		gin.H{
			"status": http.StatusCreated,
			"data":   stocks})
}

func GetStockById(c *gin.Context) {
	var stocks models.Stocks
	db := db.GetDB()

	db.Where("id = ?", c.Param("id")).First(&stocks)

	c.JSON(http.StatusCreated,
		gin.H{
			"status": http.StatusCreated,
			"data":   stocks})
}

func CreateStock(c *gin.Context) {

	sales, _ := strconv.ParseFloat(c.PostForm("sales"), 32)
	purchases, _ := strconv.ParseFloat(c.PostForm("purchases"), 32)

	stock := models.Stocks{
		Sku:         c.PostForm("sku"),
		Name:        c.PostForm("name"),
		Sales:       sales,
		Purchases:   purchases,
		CreatedDate: time.Now(),
		CreatedBy:   "admin",
		Amount:      0,
		Status:      1}

	db := db.GetDB()

	db.Save(&stock)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":     http.StatusCreated,
			"message":    "item created successfully!",
			"resourceId": stock.ID})
}

func UpdateStock(c *gin.Context) {

	var stocks models.Stocks
	sales, _ := strconv.ParseFloat(c.PostForm("sales"), 32)
	purchases, _ := strconv.ParseFloat(c.PostForm("purchases"), 32)

	content := models.Stocks{
		Sku:         c.PostForm("sku"),
		Name:        c.PostForm("name"),
		Sales:       sales,
		Purchases:   purchases,
		UpdatedDate: time.Now(),
		Amount:      0,
		Status:      1}

	db := db.GetDB()

	db.Model(&stocks).Where("id = ?", c.Param("id")).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!"})
}

func HardDeleteStock(c *gin.Context) {
	var stocks models.Stocks
	db := db.GetDB()
	db.Where("id = ?", c.Param("id")).Delete(&stocks)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item deleted successfully!"})
}

func SoftDeleteStock(c *gin.Context) {
	var stocks models.Stocks

	content := models.Stocks{
		Status: 2}

	db := db.GetDB()
	db.Model(&stocks).Where("id = ?", c.Param("id")).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!",
			"id":      c.Param("id")})
}

func ActivateStock(c *gin.Context) {
	var stocks models.Stocks

	status, _ := strconv.Atoi(c.PostForm("status"))

	content := models.Stocks{
		Status: status}

	db := db.GetDB()
	db.Model(&stocks).Where("id = ?", c.Param("id")).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!",
			"id":      c.Param("id")})
}

func ExportStock(c *gin.Context) {

	var stocks []models.Stocks
	db := db.GetDB()
	db.Where("status = ?", 1).Find(&stocks)

	b := &bytes.Buffer{}
	W := csv.NewWriter(b)

	if err := W.Write([]string{"LAPORAN STOCK BARANG"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{"SKU", "Name", "Sales", "Purchases", "Amount"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	// Content
	for _, value := range stocks {
		var record []string
		record = append(record, value.Sku)
		record = append(record, value.Name)
		record = append(record, strconv.Itoa(int(math.Round(value.Sales))))
		record = append(record, strconv.Itoa(int(math.Round(value.Purchases))))
		record = append(record, strconv.Itoa(int(value.Amount)))

		if err := W.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}

	W.Flush()
	if err := W.Error(); err != nil {
		log.Fatal(err)
	}

	c.Header("Content-Description", "Laporan Nilai Barang")
	c.Header("Content-Disposition", "attachment; filename=stock.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}

func ImportStockCsv(c *gin.Context) {

	file, err := c.FormFile("Assets")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}

	if err := ReadFile(file); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		return
	}

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item import successfully!"})

}

func ReadFile(file *multipart.FileHeader) error {
	csvfile, err := file.Open()
	if err != nil {
		return err
	}
	defer csvfile.Close()

	lines, err := csv.NewReader(csvfile).ReadAll()
	if err != nil {
		panic(err)
	}
	db := db.GetDB()

	for i := 1; i < len(lines); i++ {

		sales, _ := strconv.ParseFloat(lines[i][2], 32)
		purchases, _ := strconv.ParseFloat(lines[i][3], 32)
		amount, _ := strconv.Atoi(lines[i][4])

		stock := models.Stocks{
			Sku:         lines[i][0],
			Name:        lines[i][1],
			Sales:       sales,
			Purchases:   purchases,
			CreatedDate: time.Now(),
			CreatedBy:   lines[i][5],
			Amount:      amount,
			Status:      1,
		}

		db.Save(&stock)
	}

	return nil
}
