package controllers

import (
	"bytes"
	"encoding/csv"
	"github.com/gin-gonic/gin"
	"gobigbuddy/db"
	"gobigbuddy/models"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"
)

func GetTransaction(c *gin.Context) {

	var transaction []models.Transaction
	db := db.GetDB()

	db.Where("status = ?", 1).Find(&transaction)

	c.JSON(http.StatusCreated,
		gin.H{
			"status": http.StatusCreated,
			"data":   transaction})
}

func GetTransactionById(c *gin.Context) {
	var transaction models.Transaction
	db := db.GetDB()

	db.Where("id = ?", c.Param("id")).First(&transaction)

	c.JSON(http.StatusCreated,
		gin.H{
			"status": http.StatusCreated,
			"data":   transaction})
}

func CreateTransaction(c *gin.Context) {

	stockId, _ := strconv.Atoi(c.PostForm("stockId"))
	types, _ := strconv.Atoi(c.PostForm("types"))
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	price, _ := strconv.ParseFloat(c.PostForm("price"), 32)

	transaction := models.Transaction{
		StockId:     stockId,
		Types:       types,
		Price:       price,
		Amount:      amount,
		CreatedDate: time.Now(),
		CreatedBy:   "admin",
		Status:      1}

	db := db.GetDB()

	db.Save(&transaction)

	type Trans struct {
		Types int
		Total int
	}
	var trans1 Trans
	var trans2 Trans

	var totalAmount int
	db.Table("transactions").Select("types, sum(amount) as total").Where("stock_id = ? AND types = ?", stockId, 1).Group("types").First(&trans1)
	db.Table("transactions").Select("types, sum(amount) as total").Where("stock_id = ? AND types = ?", stockId, 2).Group("types").First(&trans2)
	totalAmount = trans1.Total - trans2.Total

	content := models.Stocks{Amount: totalAmount}
	db.Table("stocks").Where("id = ?", stockId).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":     http.StatusCreated,
			"message":    "item created successfully!",
			"resourceId": transaction.ID})
}

func UpdateTransaction(c *gin.Context) {

	var transaction models.Transaction
	stockId, _ := strconv.Atoi(c.PostForm("stockId"))
	types, _ := strconv.Atoi(c.PostForm("types"))
	amount, _ := strconv.Atoi(c.PostForm("amount"))
	price, _ := strconv.ParseFloat(c.PostForm("price"), 32)

	content := models.Transaction{
		StockId:     stockId,
		Types:       types,
		Price:       price,
		Amount:      amount,
		UpdatedDate: time.Now(),
		Status:      1}

	db := db.GetDB()
	db.Model(&transaction).Where("id = ?", c.Param("id")).Update(&content)

	type Trans struct {
		Types int
		Total int
	}
	var trans1 Trans
	var trans2 Trans

	var totalAmount int
	db.Table("transactions").Select("types, sum(amount) as total").Where("stock_id = ? AND types = ?", stockId, 1).Group("types").First(&trans1)
	db.Table("transactions").Select("types, sum(amount) as total").Where("stock_id = ? AND types = ?", stockId, 2).Group("types").First(&trans2)
	totalAmount = trans1.Total - trans2.Total

	contentStock := models.Stocks{Amount: totalAmount}
	db.Table("stocks").Where("id = ?", stockId).Update(&contentStock)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!"})
}

func HardDeleteTransaction(c *gin.Context) {
	var transaction models.Transaction
	db := db.GetDB()
	db.Where("id = ?", c.Param("id")).Delete(&transaction)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item deleted successfully!"})
}

func SoftDeleteTransaction(c *gin.Context) {
	var transaction models.Transaction

	content := models.Transaction{
		Status: 2}

	db := db.GetDB()
	db.Model(&transaction).Where("id = ?", c.Param("id")).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!",
			"id":      c.Param("id")})
}

func ActivateTransaction(c *gin.Context) {
	var transaction models.Transaction

	status, _ := strconv.Atoi(c.PostForm("status"))

	content := models.Transaction{
		Status: status}

	db := db.GetDB()
	db.Model(&transaction).Where("id = ?", c.Param("id")).Update(&content)

	c.JSON(http.StatusCreated,
		gin.H{
			"status":  http.StatusCreated,
			"message": "item updated successfully!",
			"id":      c.Param("id")})
}

func ExportSales(c *gin.Context) {

	type Salestrans struct {
		Sku         string
		Name        string
		Sales       float64
		Price       float64
		Amount      int
		CreatedDate time.Time
	}
	var salestrans []Salestrans

	db := db.GetDB()

	db.Table("stocks").Select("stocks.sku, stocks.name, stocks.sales, transactions.price, transactions.amount, transactions.created_date").Joins("JOIN transactions ON stocks.id = transactions.stock_id").Where("transactions.types = ? AND transactions.status = ?", 2, 1).Find(&salestrans)

	b := &bytes.Buffer{}
	W := csv.NewWriter(b)

	if err := W.Write([]string{"LAPORAN PENJUALAN BARANG"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{"SKU", "Nama Barang", "Standar Harga Jual", "Harga Jual", "Jumlah Barang", "Tanggal Penjualan"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	// Content
	for _, value := range salestrans {
		var record []string

		var CreatedDate string = value.CreatedDate.Format("2006-01-02 15:04:05")

		record = append(record, value.Sku)
		record = append(record, value.Name)
		record = append(record, strconv.Itoa(int(math.Round(value.Sales))))
		record = append(record, strconv.Itoa(int(math.Round(value.Price))))
		record = append(record, strconv.Itoa(int(value.Amount)))
		record = append(record, CreatedDate)

		if err := W.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}

	W.Flush()
	if err := W.Error(); err != nil {
		log.Fatal(err)
	}

	c.Header("Content-Description", "Laporan Nilai Barang")
	c.Header("Content-Disposition", "attachment; filename=sales_stock.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}

func ExportPurchases(c *gin.Context) {
	type Purchasestrans struct {
		Sku         string
		Name        string
		Purchases   float64
		Price       float64
		Amount      int
		CreatedDate time.Time
	}
	var purchasestrans []Purchasestrans

	db := db.GetDB()

	db.Table("stocks").Select("stocks.sku, stocks.name, stocks.purchases, transactions.price, transactions.amount, transactions.created_date").Joins("JOIN transactions ON stocks.id = transactions.stock_id").Where("transactions.types = ? AND transactions.status = ?", 1, 1).Find(&purchasestrans)

	b := &bytes.Buffer{}
	W := csv.NewWriter(b)

	if err := W.Write([]string{"LAPORAN PEMBELIAN BARANG"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{"SKU", "Nama Barang", "Standar Harga Beli", "Harga Beli", "Jumlah Barang", "Tanggal Pembelian"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	// Content
	for _, value := range purchasestrans {
		var record []string

		var CreatedDate string = value.CreatedDate.Format("2006-01-02 15:04:05")

		record = append(record, value.Sku)
		record = append(record, value.Name)
		record = append(record, strconv.Itoa(int(math.Round(value.Purchases))))
		record = append(record, strconv.Itoa(int(math.Round(value.Price))))
		record = append(record, strconv.Itoa(int(value.Amount)))
		record = append(record, CreatedDate)

		if err := W.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}

	W.Flush()
	if err := W.Error(); err != nil {
		log.Fatal(err)
	}

	c.Header("Content-Description", "Laporan Nilai Barang")
	c.Header("Content-Disposition", "attachment; filename=purchases_stock.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}

func ExportProfit(c *gin.Context) {

	var stocks []models.Stocks

	db := db.GetDB()
	db.Where("status = ?", 1).Find(&stocks)

	b := &bytes.Buffer{}
	W := csv.NewWriter(b)

	if err := W.Write([]string{"LAPORAN KEUNTUNGAN PENJUALAN"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	if err := W.Write([]string{"SKU", "Nama Barang", "Standar Harga Beli", "Standar Harga Jual", "Stock Barang", "Barang Terjual", "Sisa Barang", "Omzet", "Profit"}); err != nil {
		log.Fatalln("error writing record to csv:", err)
	}

	// Content
	for _, value := range stocks {
		var record []string

		type Trans struct{ Total int }
		var trans Trans
		var omzet float64
		var profit float64
		var totalStock int

		db.Table("transactions").Select("sum(amount) as total").Where("stock_id = ? AND types = ?", value.ID, 2).Group("types").First(&trans)

		amountSales := float64(trans.Total)

		omzet = amountSales * value.Sales
		profit = omzet - (amountSales * value.Purchases)
		totalStock = trans.Total + value.Amount

		record = append(record, value.Sku)
		record = append(record, value.Name)
		record = append(record, strconv.Itoa(int(math.Round(value.Purchases))))
		record = append(record, strconv.Itoa(int(math.Round(value.Sales))))
		record = append(record, strconv.Itoa(int(totalStock)))
		record = append(record, strconv.Itoa(int(trans.Total)))
		record = append(record, strconv.Itoa(int(value.Amount)))
		record = append(record, strconv.Itoa(int(math.Round(omzet))))
		record = append(record, strconv.Itoa(int(math.Round(profit))))

		if err := W.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}

	W.Flush()
	if err := W.Error(); err != nil {
		log.Fatal(err)
	}

	c.Header("Content-Description", "Laporan Nilai Barang")
	c.Header("Content-Disposition", "attachment; filename=profitloss.csv")
	c.Data(http.StatusOK, "text/csv", b.Bytes())
}
