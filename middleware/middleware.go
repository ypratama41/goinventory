package models

import (
	jwt_lib "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"

	"time"
)

var (
	Mysupersecretpassword = "passwordjwtgolang"
)

func CreateToken(c *gin.Context) {
	// Create the token
	token := jwt_lib.New(jwt_lib.GetSigningMethod("HS256"))
	// Set some claims
	token.Claims = jwt_lib.MapClaims{
		"Id":  "Christopher",
		"exp": time.Now().Add(time.Hour * 10000).Unix(),
	}
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString([]byte(Mysupersecretpassword))
	if err != nil {
		c.JSON(500, gin.H{"message": "Could not generate token"})
	}
	c.JSON(200, gin.H{"token": tokenString})
}
